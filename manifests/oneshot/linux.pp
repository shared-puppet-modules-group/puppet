# helper for puppet::oneshot, inheriting puppet::linux
# so it can override File['/etc/cron.d/puppetd']
class puppet::oneshot::linux inherits puppet::linux {

  File['/etc/cron.d/puppetd'] {
    ensure => absent,
  }

}
