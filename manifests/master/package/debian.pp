class puppet::master::package::debian inherits puppet::master::package::base {

  if versioncmp($::operatingsystemmajrelease,'9') >= 0 {
    Package['puppetmaster']{
      name => 'puppet-master'
    }
  }
  else {
    package { 'puppetmaster-common':
      ensure => present,
    }
    Package['puppetmaster']{
      require => Package['puppetmaster-common']
    }
  }
}
